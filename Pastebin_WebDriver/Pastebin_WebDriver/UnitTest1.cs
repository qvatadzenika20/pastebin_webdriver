using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Pastebin_WebDriver
{

    public class PastebinPage
    {
        private readonly IWebDriver driver;
        private readonly WebDriverWait wait;

        public PastebinPage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        public void OpenPage()
        {
            driver.Navigate().GoToUrl("https://pastebin.com/");
        }

        public void CreateNewPaste(string code, string pasteName, string expiration)
        {
            IWebElement codeTextArea = wait.Until(d => d.FindElement(By.Id("postform-text")));
            codeTextArea.SendKeys(code);

            IWebElement expirationDropdown = driver.FindElement(By.Id("select2-postform-expiration-container"));
            expirationDropdown.Click();

            IWebElement tenMinutesOption = wait.Until(d => d.FindElement(By.XPath("//li[text()='10 Minutes']")));
            tenMinutesOption.Click();

            IWebElement pasteNameField = driver.FindElement(By.Id("postform-name"));
            pasteNameField.SendKeys(pasteName);

            IWebElement createButton = driver.FindElement(By.CssSelector("button[type='submit']"));
            createButton.Click();
        }

        public bool IsPasteCreated()
        {
            return wait.Until(d => d.Url.Contains("pastebin.com/"));
        }
    }


    public class PastebinTest
    {
        private IWebDriver driver;

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
        }

        [TearDown]
        public void TearDown()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }

        [Test]
        public void TestCreateNewPaste()
        {
            var pastebinPage = new PastebinPage(driver);
            pastebinPage.OpenPage();

            pastebinPage.CreateNewPaste("Hello from WebDriver", "helloweb", "10 Minutes");

            Assert.IsTrue(pastebinPage.IsPasteCreated(), "Paste creation failed.");

        }
    }
}